# Porte pour Ultimaker

Une 'porte' pour fermer l'avant de nos Ultimaker 2+ et 3

Matériel nécesaire :
 - plaque de PMMA de 3mm d'épaisseur à découper au laser
 - 4 vis M3x19mm + 4 écrous + 4 rondelles (en option)